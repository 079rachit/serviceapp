import { NgModule, ErrorHandler } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { IonicApp, IonicModule, IonicErrorHandler } from 'ionic-angular';
import { MyApp } from './app.component';
import { ApiProvider } from '../Api/api';
import { HttpModule } from '@angular/http';

import { AboutPage } from '../pages/about/about';
import { ContactPage } from '../pages/contact/contact';
import { HomePage } from '../pages/home/home';
import { TabsPage } from '../pages/tabs/tabs';
import { LoginPage } from '../pages/login/login';
import { ForgotPage } from '../pages/forgot/forgot';
import { OtpPage } from '../pages/otp/otp';
import { MobilePage } from '../pages/mobile/mobile';
import { ChangepasswordPage } from '../pages/changepassword/changepassword';

import { StatusBar } from '@ionic-native/status-bar';
import { SplashScreen } from '@ionic-native/splash-screen';
import { LoginPageModule } from '../pages/login/login.module';
import { ForgotPageModule } from '../pages/forgot/forgot.module';
import { OtpPageModule } from '../pages/otp/otp.module';
import { MobilePageModule } from '../pages/mobile/mobile.module';
import { ChangepasswordPageModule } from '../pages/changepassword/changepassword.module';
import { SurveyPageModule } from '../pages/survey/survey.module';
import { Geolocation } from '@ionic-native/geolocation';
import { NewloginPageModule } from '../pages/newlogin/newlogin.module';

@NgModule({
  declarations: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    // LoginPage,
    // ForgotPage,
    // OtpPage,
    // MobilePage,
    // ChangepasswordPage

  ],
  imports: [
    BrowserModule,
    IonicModule.forRoot(MyApp),
    HttpModule,
    LoginPageModule,
    ForgotPageModule,
    OtpPageModule,
    MobilePageModule,
    ChangepasswordPageModule,
    SurveyPageModule,
    NewloginPageModule
  ],
  bootstrap: [IonicApp],
  entryComponents: [
    MyApp,
    AboutPage,
    ContactPage,
    HomePage,
    TabsPage,
    LoginPage,
    ForgotPage,
    OtpPage,
    MobilePage,
    ChangepasswordPage
  ],
  providers: [
    StatusBar,
    SplashScreen,
    {provide: ErrorHandler, useClass: IonicErrorHandler},
    ApiProvider,
    Geolocation
  ]
})
export class AppModule {}
