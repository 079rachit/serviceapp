import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { Observable } from 'rxjs/Rx';
import 'rxjs/add/operator/map';

let BASE_URL = 'http://122.160.196.113:8080/API/api/';
// http://dev.webmobrilmedia.com/idied_app/api/login.php?username=aj@gmail.com&password=123456
//http://dev.webmobrilmedia.com/pccar/apis/api_login
@Injectable()
export class ApiProvider {
  text1: any;
  imageUrl1: any;
  constructor(public _http: Http,) { }
  //http://122.160.196.113:8080/API/api/Login/Login	
    loginApi(MobileNo,Password): Observable<any> {
        const formData = new FormData();
        formData.append('MobileNo', MobileNo);
        formData.append('Password', Password);

        let api_url_login = BASE_URL + 'Login/Login';
        return this._http.post(api_url_login,formData );
      }
      // http://122.160.196.113:8080/API/api/Login/GetOTP

      getOTPApi(MobileNo): Observable<any> {
        const formData = new FormData();
        formData.append('MobileNo', MobileNo);

        let api_url_login = BASE_URL + 'Login/GetOTP';
        return this._http.post(api_url_login,formData );
      }

      

      //http://122.160.196.113:8080/API/api/Login/ValidateOTP
      ValidateOTP(MobileNo): Observable<any> {
        const formData = new FormData();
        formData.append('MobileNo', MobileNo);

        let api_url_login = BASE_URL + 'Login/ValidateOTP';
        return this._http.post(api_url_login,formData );
      }
      //http://122.160.196.113:8080/API/api/Login/UpdateUser
      
      UpdateUser(ID,Password,ProfilePic): Observable<any> {
        const formData = new FormData();
        formData.append('ID', ID);
        formData.append('Password', Password);
        formData.append('ProfilePic', ProfilePic);

        let api_url_login = BASE_URL + 'Login/UpdateUser';
        return this._http.post(api_url_login,formData );
      }

      //http://122.160.196.113:8080/API/api/Survey/AddSurvey
      AddSurvey(Userid,SiteID,SiteName,SiteAddress,Cust_name,Cust_Code,AC_FCU_make,AC_FCU_Rating,
        AC_FCU_Remarks,Batterybank_make,Batterybank_Rating,Batterybank_Remarks,
        Battery_Current,Battery_Remarks,Battery_Voltage,BTS_make,BTS_Rating,BTS_Remarks,
        DG_Current,DG_make,DG_Rating,DG_Remarks,DG_Voltage,EB_Current,EB_Remarks,
        EB_Voltage,Place_Lat,Place_Long,PowerPanel_make,PowerPanel_Rating,
        PowerPanel_Remarks,RoomTemp,RoomTemp_Remarks,Site_Heat_Load_make,
        Site_Heat_Load_Rating,Site_Heat_Load_Remarks,Site_Type,SMPS_make,
        SMPS_Rating,SMPS_Remarks,Solar_Current,Solar_Remarks,Solar_Voltage): Observable<any> {
        const formData = new FormData();
        formData.append('SiteID', SiteID);
        formData.append('SiteName', SiteName);
        formData.append('SiteAddress', SiteAddress);
        formData.append('Cust_name',Cust_name);
        formData.append('Cust_Code',Cust_Code);
        formData.append('AC_FCU_make',AC_FCU_make);
        formData.append('AC_FCU_Rating',AC_FCU_Rating);
        formData.append('AC_FCU_Remarks',AC_FCU_Remarks);
        formData.append('Userid',Userid);
        formData.append('Batterybank_make',Batterybank_make);
        formData.append('Batterybank_Rating',Batterybank_Rating);
        formData.append('Batterybank_Remarks',Batterybank_Remarks);
        formData.append('Battery_Current',Battery_Current);
        formData.append('Battery_Remarks',Battery_Remarks);
        formData.append('Battery_Voltage',Battery_Voltage);
        formData.append('BTS_make',BTS_make);
        formData.append('BTS_Rating',BTS_Rating);
        formData.append('BTS_Remarks',BTS_Remarks);
        formData.append('DG_Current',DG_Current);
        formData.append('DG_make',DG_make);
        formData.append('DG_Rating',DG_Rating);
        formData.append('DG_Remarks',DG_Remarks);
        formData.append('DG_Voltage',DG_Voltage);
        formData.append('EB_Current',EB_Current);
        formData.append('EB_Remarks',EB_Remarks);
        formData.append('EB_Voltage',EB_Voltage);
        formData.append('Place_Lat',Place_Lat);
        formData.append('Place_Long',Place_Long);
        formData.append('PowerPanel_make',PowerPanel_make);
        formData.append('PowerPanel_Rating',PowerPanel_Rating);
        formData.append('PowerPanel_Remarks',PowerPanel_Remarks);
        formData.append('RoomTemp',RoomTemp);
        formData.append('RoomTemp_Remarks',RoomTemp_Remarks);
        formData.append('Site_Heat_Load_make',Site_Heat_Load_make);
        formData.append('Site_Heat_Load_Rating',Site_Heat_Load_Rating);
        formData.append('Site_Heat_Load_Remarks',Site_Heat_Load_Remarks);
        formData.append('Site_Type',Site_Type);
        formData.append('SMPS_make',SMPS_make);
        formData.append('SMPS_Rating',SMPS_Rating);
        formData.append('SMPS_Remarks',SMPS_Remarks);
        formData.append('Solar_Current',Solar_Current);
        formData.append('Solar_Remarks',Solar_Remarks);
        formData.append('Solar_Voltage',Solar_Voltage);

        let api_url_login = BASE_URL + 'Survey/AddSurvey';
        return this._http.post(api_url_login,formData );
      }

//http://dev.webmobrilmedia.com/idied_app/api/signup.php?first_name=amit&last_name=bhoj
//&email=amit@webmobril.com&phone=9971682880&password=123456&encrptpassword=123456

registerApi(firstname,lastname,email,phone,password): Observable<any> {
        const formData = new FormData();
        formData.append('first_name', firstname);
        formData.append('last_name', lastname);
        formData.append('email', email);
        formData.append('phone', phone);
        formData.append('password', password);

        let api_url_register = BASE_URL + 'signup.php';
        return this._http.post(api_url_register,formData );
      }
      //http://dev.webmobril.services/pccar/apis/api_addclaimimages
      //user_id,data[image],image_caption,claim_number
      api_addclaimimages(user_id,leftFront): Observable<any> {
        const formData = new FormData();
        formData.append('user_id', user_id);
        formData.append('leftFront', leftFront);

        let api_url_reset = BASE_URL + 'profile_update.php';
        return this._http.post(api_url_reset,formData );
      }



      getQuestionsApi(chapter_id): Observable<any> {
        const formData = new FormData();
        formData.append('chapter_id', chapter_id);
        let api_url_register = BASE_URL + 'get_questions.php';
        return this._http.post(api_url_register,formData );
      }
        
      getCategoriesApi(): Observable<any> {
        const formData = new FormData();
        let api_url_register = BASE_URL + 'get_category.php';
        return this._http.post(api_url_register,formData );
      }
      getSubCategoriesApi(): Observable<any> {
        const formData = new FormData();
        let api_url_register = BASE_URL + 'get_sub_category.php';
        return this._http.post(api_url_register,formData );
      }
      getProducts(): Observable<any> {
        const formData = new FormData();
        let api_url_register = BASE_URL + 'getproduct.php';
        return this._http.post(api_url_register,formData );
      }

      getStations(): Observable<any> {
        const formData = new FormData();
        let api_url_register = BASE_URL + 'get_station.php';
        return this._http.post(api_url_register,formData );
      }

      getSmallDip(): Observable<any> {
        const formData = new FormData();
        let api_url_register = BASE_URL + 'get_small_dip.php';
        return this._http.post(api_url_register,formData );
      }

      getDebters(): Observable<any> {
        const formData = new FormData();
        let api_url_register = BASE_URL + 'get_debtor.php';
        return this._http.post(api_url_register,formData );
      }

      getSupplier(): Observable<any> {
        const formData = new FormData();
        let api_url_register = BASE_URL + 'get_supplier.php';
        return this._http.post(api_url_register,formData );
      }


      // http://dev.webmobril.services/stationpro/api/save_all_data.php?meter_product_id=14
      // &opening_meter=12222.60&closing_meter=123456.50&fuel_product_sale_id=16&
      // fuel_sale_qty=2&fuel_sale_price=600&fuel_sale_amount=9000&fuel_product_supply_id=15&
      // fuel_supply_quantity=3&fuel_supply_station_id=1&fuel_transfer_station_id=1&
      // fuel_transfer_quantity=4&fuel_transfer_price=3000&fuel_tansfer_amount=8000&
      // fuel_product_tank_dipping_id=16&dipping_id=1&product_dipping_stock_in_litre=6&
      // other_category_income_id=1&other_sub_category_income_id=1&
      // other_income_quantity=8&other_income_amount=9000&debtor_account_id=3&
      // debtor_account_quantity=8&debtor_amount_paid=500&expenses_category_id=1&
      // expenses_description=kdkekkkkek&expenses_amount=400&total_cash_collected=8000&
      // cash_collected_banked=9000&user_id=1

      saveAllData(user_id,meter_product_id,opening_meter,closing_meter,fuel_product_sale_id,
        fuel_sale_qty,fuel_sale_price,fuel_sale_amount,fuel_product_supply_id,fuel_supply_quantity
        ,fuel_supply_station_id,fuel_transfer_station_id,fuel_transfer_quantity,
        fuel_transfer_price,fuel_tansfer_amount,fuel_product_tank_dipping_id,dipping_id,
        product_dipping_stock_in_litre,other_category_income_id,other_sub_category_income_id,
        other_income_quantity,other_income_amount,debtor_account_id,debtor_account_quantity,
        debtor_amount_paid,expenses_category_id,expenses_description,expenses_amount,cash_collected
        ,banked,station_id): Observable<any> {
        const formData = new FormData();
        formData.append('user_id', user_id);
        formData.append('product_id', meter_product_id);
        formData.append('opening_meter', opening_meter);
        formData.append('closing_meter', closing_meter);
        formData.append('fule_product_id', fuel_product_sale_id);
        formData.append('fule_quantity', fuel_sale_qty);
        formData.append('fule_price', fuel_sale_price);
        formData.append('fule_amount', fuel_sale_amount);
        formData.append('fuel_product_supply_id', fuel_product_supply_id);
        formData.append('supply_quantity', fuel_supply_quantity);
        formData.append('supply_station_id', fuel_supply_station_id);
        formData.append('station_transfer_id', fuel_transfer_station_id);
        formData.append('transfer_quantity', fuel_transfer_quantity);
        formData.append('transfer_price', fuel_transfer_price);
        formData.append('tansfer_amount', fuel_tansfer_amount);
        formData.append('dipping_product_id', fuel_product_tank_dipping_id);
        formData.append('dips_id', dipping_id);
        formData.append('dipping_quantity', product_dipping_stock_in_litre);
        formData.append('other_category_id', other_category_income_id);
        formData.append('other_sub_category_id', other_sub_category_income_id);
        formData.append('other_quantity', other_income_quantity);
        formData.append('other_amount', other_income_amount);

        formData.append('debtors_id', debtor_account_id);
        formData.append('debtors_quantity', debtor_account_quantity);
        formData.append('debtors_amount', debtor_amount_paid);
        formData.append('expenses_category_id', expenses_category_id);
        formData.append('expenses_descript', expenses_description);
        formData.append('expenses_amount', expenses_amount);
        formData.append('cash_collected', cash_collected);
        formData.append('banked', banked);



        formData.append('cash', '9');
        formData.append('meter_reading', '1');
        formData.append('fule_sales', '2');
        formData.append('fule_supply', '3');
        formData.append('transfer', '4');
        formData.append('dipping', '5');
        formData.append('income', '6');
        formData.append('debtors', '7');
        formData.append('expenses', '8');
        formData.append('station_id', station_id);


        let api_url_register = BASE_URL + 'save_all_data.php';
        return this._http.post(api_url_register,formData );
      }

      resetApi(user_id,old_pass,new_pass): Observable<any> {
        const formData = new FormData();
        formData.append('user_id', user_id);
        formData.append('old_pass', old_pass);
        formData.append('new_pass', new_pass);
        let api_url_reset = BASE_URL + 'reset_password.php';
        return this._http.post(api_url_reset,formData );
      }

      forgotApi(email): Observable<any> {
        const formData = new FormData();
        formData.append('email', email);
        let api_url_reset = BASE_URL + 'forget_password.php';
        return this._http.post(api_url_reset,formData );
      }
      getProfileApi(user_id): Observable<any> {
        const formData = new FormData();
        formData.append('user_id', user_id);
        let api_url_reset = BASE_URL + 'get_profile.php';
        return this._http.post(api_url_reset,formData );
      }
      get_all_data(user_id,station_id): Observable<any> {
        const formData = new FormData();
        formData.append('user_id', user_id);
        formData.append('station_id', station_id);
        let api_url_reset = BASE_URL + 'get_all_data_new.php';
        return this._http.post(api_url_reset,formData );
      }

      get_data_by_id(id): Observable<any> {
        const formData = new FormData();
        formData.append('id', id);
        let api_url_reset = BASE_URL + 'get_all_data_detail.php';
        return this._http.post(api_url_reset,formData );
      }
      // http://dev.webmobril.services/stationpro/api/save_order.php?order_quantity=6,9,10&product_id=14,15,16&supplier_id=1,2,3&
      // station_id=14&user_id=11
      makeOrder(user_id,product,quantity,supplier): Observable<any> {
        const formData = new FormData();
        formData.append('user_id', user_id);
        formData.append('product_id', product);
        formData.append('order_quantity', quantity);
        formData.append('supplier_id', supplier);
        formData.append('station_id', '1');

        let api_url_register = BASE_URL + 'save_order.php';
        return this._http.post(api_url_register,formData );
      }
      updateProfileApi(user_id,first_name,last_name,email,phone,city,state,country): Observable<any> {
        const formData = new FormData();
        formData.append('user_id', user_id);
        formData.append('first_name', first_name);
        formData.append('last_name', last_name);
        formData.append('email', email);
        formData.append('phone', phone);
        formData.append('city', city);
        formData.append('state', state);
        formData.append('country', country);
        formData.append('file_url', "");
        let api_url_register = BASE_URL + 'profile_update.php';
        return this._http.post(api_url_register,formData );
      }


}
