import { Component } from '@angular/core';
import { NavController,Tabs } from 'ionic-angular';
import { SurveyPage } from '../survey/survey';
import { TabsPage } from '../tabs/tabs';
import { Geolocation } from '@ionic-native/geolocation';

@Component({
  selector: 'page-home',
  templateUrl: 'home.html'
})
export class HomePage {
  name:any;
  latitude:any;
  longitude:any;
  constructor(public navCtrl: NavController,public tab:Tabs,private geolocation: Geolocation) {
    // this.tab.setTabbarHidden(false);
    this.name = localStorage.getItem('Name');
    console.log(name);

    this.geolocation.getCurrentPosition().then((resp) => {
      // resp.coords.latitude
      // resp.coords.longitude
     }).catch((error) => {
       console.log('Error getting location', error);
     });
     
     let watch = this.geolocation.watchPosition();
     watch.subscribe((data) => {
      // data can be a set of coordinates, or an error (if an error occurred).
       data.coords.latitude
       data.coords.longitude
       this.latitude = data.coords.latitude;
       this.longitude = data.coords.longitude;
       localStorage.setItem('latitude',this.latitude);
       localStorage.setItem('longitude',this.longitude);
      console.log(this.latitude);
      console.log(this.longitude);

     });
  }

  ionViewDidEnter()
  {
    // this.navCtrl.setRoot(TabsPage);
  }

  startSurvey()
  {
    // this.navCtrl.setRoot(SurveyPage);
    this.navCtrl.push(SurveyPage);
  }

}
