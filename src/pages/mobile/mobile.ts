import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController,AlertController, ToastController,LoadingController } from 'ionic-angular';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { ApiProvider } from '../../Api/api';
import { OtpPage } from '../otp/otp';
import { ChangepasswordPageModule } from '../changepassword/changepassword.module';


/**
 * Generated class for the MobilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-mobile',
  templateUrl: 'mobile.html',
})
export class MobilePage {
  myForm: FormGroup;
  mobile:any;
  loading:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public menu: MenuController,
    public alertCtrl:AlertController, private toastCtrl:ToastController,
    public loadingCtrl: LoadingController,public service:ApiProvider) {
    this.menu.swipeEnable(false);
    this.myForm = new FormGroup({ mobile: new FormControl(null, Validators.compose([Validators.required,Validators.minLength(10)])) });
  }
  
  validate(): boolean {
    if (this.myForm.valid) {
      return true;
    }
    // figure out the error message
    let errorMsg = '';
    // validate each field
    let control = this.myForm.controls['mobile'];
    if (!control.valid) {
      if (control.errors['required']) {
        errorMsg = 'Provide a mobile please';
        this.showAlert(errorMsg);
        return;        
      } else if (control.errors['minlength']) {
        errorMsg = 'The mobile must have at least 10 numbers';
        this.showAlert(errorMsg);
        return;        
      }
    }
    return false;
  }


  onSubmit(form: any) {
    if (this.validate()) {
      this.showLoader('Please wait...');

      // process the data
      this.service.getOTPApi( this.mobile ).map(res => <any>res.json())
      .subscribe(data => {
        this.loading.dismiss();
     console.log(data);
          //  this.navCtrl.setRoot(ChangepasswordPageModule);


        if(data.OTP == "0"){

          // this.toast(data.responseMessage +'Your Otp is:'+data.otp);
          this.toast("Invalid Mobile No.!!")
        }
        else
        {
          localStorage.setItem('otp',data.OTP);
          localStorage.setItem('mobile',this.mobile);

          this.toast(data.OTP);
          this.navCtrl.setRoot(OtpPage);
        }
      },
      errData =>{
        this.toast("Please Check Your Internet Connection");
        this.loading.dismiss();

       });
    }
  }


  showAlert(msg) {
    let alert = this.alertCtrl.create({
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }
  toast(msg){
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }
  showLoader(msg) {
    this.loading = this.loadingCtrl.create({
      content: msg
    });
    this.loading.present();
  }
  forgotPassword()
  {
  }
  backBtn()
  {
  }
}
