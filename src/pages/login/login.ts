import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController,AlertController, ToastController,LoadingController,Tabs } from 'ionic-angular';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { ApiProvider } from '../../Api/api';
import { TabsPage } from '../tabs/tabs';
import { ForgotPage } from '../forgot/forgot';

/**
 * Generated class for the LoginPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-login',
  templateUrl: 'login.html',
})
export class LoginPage {
  myForm: FormGroup;
  username:any;
  password:any;
  pattern = /^\w+@[a-zA-Z_]+?\.[a-zA-Z]{2,3}$/; 
  loading:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public menu: MenuController,
    public alertCtrl:AlertController, private toastCtrl:ToastController,
    public loadingCtrl: LoadingController,public service:ApiProvider,public tab:Tabs) {
    this.menu.swipeEnable(false);
     this.tab.setTabbarHidden(true);

    this.myForm = new FormGroup({ username: new FormControl(null, Validators.compose([Validators.required, Validators.minLength(10)])),password: new FormControl(null, Validators.compose([Validators.required, Validators.minLength(5)])) });
    // localStorage.removeItem('ID');
    localStorage.setItem('ID',"login");

  }
  
  validate(): boolean {
    if (this.myForm.valid) {
      return true;
    }
    // figure out the error message
    let errorMsg = '';
    // validate each field
    let control = this.myForm.controls['username'];
    let control1 = this.myForm.controls['password'];
    if (!control.valid) {
      if (control.errors['required']) {
        errorMsg = 'Provide a mobile please';
        this.showAlert(errorMsg);
        return;        
      }  else if (control1.errors['minlength']) {
        errorMsg = 'The mobile must have at least 10 numbers';
        this.showAlert(errorMsg);
        return;        
      }
    }
    // validate password ...
    if (!control1.valid) {
      if (control1.errors['required']) {
        errorMsg = 'Provide a password please';
        this.showAlert(errorMsg);
        return;        
      } else if (control1.errors['minlength']) {
        errorMsg = 'The password must have at least 5 characters';
        this.showAlert(errorMsg);
        return;        
      }
    }
    return false;
  }


  onSubmit(form: any) {
    if (this.validate()) {
      this.showLoader('Please wait...');

      // process the data
      this.service.loginApi( this.username,this.password ).map(res => <any>res.json())
      .subscribe(data => {
        this.loading.dismiss();
     console.log(data);
        if(data.Status == "pass"){
          localStorage.setItem('ID',data.ID);
          localStorage.setItem('Name',data.Name);
          localStorage.setItem('User_Type',data.User_Type);
          localStorage.setItem('User_name',data.User_name);
          localStorage.setItem('Role_id',data.Role_id);
          localStorage.setItem('Email_id',data.Email_id);
          localStorage.setItem('Emp_Code',data.Emp_Code);
        this.username = "";
          this.password = "";
          this.tab.setTabbarHidden(false);

          this.navCtrl.setRoot(TabsPage);

          // this.toast(data.responseMessage +'Your Otp is:'+data.otp);
          this.toast("User Logged In Successfully!!")
        }
        else
        {
          this.toast("Login Failed");
        }
      },
      errData =>{
        this.toast("Please Check Your Internet Connection");
        this.loading.dismiss();

       });
    }
  }


  showAlert(msg) {
    let alert = this.alertCtrl.create({
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }
  toast(msg){
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 1000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }
  showLoader(msg) {
    this.loading = this.loadingCtrl.create({
      content: msg
    });
    this.loading.present();
  }
  forgotPassword()
  {
    this.navCtrl.setRoot(ForgotPage);

  }
  backBtn()
  {
  }
}