import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController,AlertController, ToastController,LoadingController } from 'ionic-angular';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { ApiProvider } from '../../Api/api';
import { ChangepasswordPage } from '../changepassword/changepassword';

/**
 * Generated class for the OtpPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-otp',
  templateUrl: 'otp.html',
})
export class OtpPage {
  myForm: FormGroup;
  otp:any;
  loading:any;
  oldOTP:any;
  constructor(public navCtrl: NavController, public navParams: NavParams,public menu: MenuController,
    public alertCtrl:AlertController, private toastCtrl:ToastController,
    public loadingCtrl: LoadingController,public service:ApiProvider) {
    this.menu.swipeEnable(false);
    this.myForm = new FormGroup({ otp: new FormControl(null, Validators.compose([Validators.required])) });

  }
  
  validate(): boolean {
    if (this.myForm.valid) {
      return true;
    }
    // figure out the error message
    let errorMsg = '';
    // validate each field
    let control = this.myForm.controls['otp'];
    if (!control.valid) {
      if (control.errors['required']) {
        errorMsg = 'Provide a otp please';
        this.showAlert(errorMsg);
        return;        
      }
    }
    return false;
  }


  onSubmit(form: any) {
    if (this.validate()) {

      let oldOTP = localStorage.getItem('otp');

      if (oldOTP == this.otp)
      {
        this.showLoader('Please wait...');
        // process the data
        let mobile = localStorage.getItem('mobile');

        this.service.ValidateOTP( mobile ).map(res => <any>res.json())
        .subscribe(data => {
          this.loading.dismiss();
       console.log(data);


          if(data.Status == "pass"){
            // localStorage.setItem('ID',data.ID);
            localStorage.setItem('Name',data.Name);
            localStorage.setItem('User_Type',data.User_Type);
            localStorage.setItem('User_name',data.User_name);
            localStorage.setItem('Role_id',data.Role_id);
            localStorage.setItem('Email_id',data.Email_id);
            localStorage.setItem('Emp_Code',data.Emp_Code);

            this.toast("OTP Verified Successfully!!")

            this.navCtrl.setRoot(ChangepasswordPage, {
              id: data.ID
            });
     
       
            // this.toast(data.responseMessage +'Your Otp is:'+data.otp);
          }
          else
          {
            this.toast(data.message);
          }
        },
        errData =>{
          this.toast("Please Check Your Internet Connection");
          this.loading.dismiss();
  
         });
      }
      else
      {
        this.toast("Wrong OTP Entered!!")

      }
     
    }
  }


  showAlert(msg) {
    let alert = this.alertCtrl.create({
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }
  toast(msg){
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 1000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }
  showLoader(msg) {
    this.loading = this.loadingCtrl.create({
      content: msg
    });
    this.loading.present();
  }
  forgotPassword()
  {
  }
  backBtn()
  {
  }
}

