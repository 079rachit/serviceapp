import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,Tabs,MenuController,AlertController, ToastController,
  LoadingController } from 'ionic-angular';
import { TabsPage } from '../tabs/tabs';
import { ApiProvider } from '../../Api/api';

/**
 * Generated class for the SurveyPage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-survey',
  templateUrl: 'survey.html',
})
export class SurveyPage {
  loading:any;
  SiteID:any;
  SiteName:any;
  SiteAddress:any;
  Cust_name:any;
  Cust_Code:any;
  AC_FCU_make:any;
  AC_FCU_Rating:any;
  AC_FCU_Remarks:any;
  Batterybank_make:any;
  Batterybank_Rating:any;
  Batterybank_Remarks:any;
  Battery_Current:any;
  Battery_Remarks:any;
  Battery_Voltage:any;
  BTS_make:any;
  BTS_Rating:any;
  BTS_Remarks:any;
  DG_Current:any;
  DG_make:any;
  DG_Rating:any;
  DG_Remarks:any;
  DG_Voltage:any;
  EB_Current:any;
  EB_Remarks:any;
  EB_Voltage:any;
  Place_Lat:any;
  Place_Long:any;
  PowerPanel_make:any;
  PowerPanel_Rating:any;
  PowerPanel_Remarks:any;
  RoomTemp:any;
  RoomTemp_Remarks:any;
  Site_Heat_Load_make:any;
  Site_Heat_Load_Rating:any;
  Site_Heat_Load_Remarks:any;
  Site_Type:any;
  SMPS_make:any;
  SMPS_Rating:any;
  SMPS_Remarks:any;
  Solar_Current:any;
  Solar_Remarks:any;
  Solar_Voltage:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public tab:Tabs,public menu: MenuController,
    public alertCtrl:AlertController, private toastCtrl:ToastController,
    public loadingCtrl: LoadingController,public service:ApiProvider) {
  }

  ionViewDidLoad() {
    console.log('ionViewDidLoad SurveyPage');
  }

  back()
  {
    this.navCtrl.setRoot(TabsPage);
  }

  submitSurvey()
  {
    console.log(this.SiteID);
    if(this.SiteID==undefined)
    {
      this.showAlert("Site ID is Blank");
      return;
    } 
    if(this.SiteName == undefined)
    {
      this.showAlert("Site Name is Blank");
      return;
    } 

    if(this.SiteAddress == undefined)
    {
      this.showAlert("Site Address is Blank");
      return;
    } 

    if(this.SiteAddress == undefined)
    {
      this.showAlert("Site Address is Blank");
      return;
    } 

    if(this.Cust_name == undefined)
    {
      this.showAlert("Customer name is Blank");
      return;
    } 
    if(this.Cust_Code == undefined)
    {
      this.showAlert("Customer code is Blank");
      return;
    } 



    this.showLoader('Please wait...');
     let userId = localStorage.getItem("ID");    
     this.Place_Lat = localStorage.getItem("latitude");    
     this.Place_Long = localStorage.getItem("longitude");    

    // process the data
    this.service.AddSurvey( userId,this.SiteID,this.SiteName,this.SiteAddress,this.Cust_name,this.Cust_Code,this.AC_FCU_make,this.AC_FCU_Rating,
      this.AC_FCU_Remarks,this.Batterybank_make,this.Batterybank_Rating,this.Batterybank_Remarks,
      this.Battery_Current,this.Battery_Remarks,this.Battery_Voltage,this.BTS_make,this.BTS_Rating,this.BTS_Remarks,
      this.DG_Current,this.DG_make,this.DG_Rating,this.DG_Remarks,this.DG_Voltage,this.EB_Current,this.EB_Remarks,
      this.EB_Voltage,this.Place_Lat,this.Place_Long,this.PowerPanel_make,this.PowerPanel_Rating,
      this.PowerPanel_Remarks,this.RoomTemp,this.RoomTemp_Remarks,this.Site_Heat_Load_make,
      this.Site_Heat_Load_Rating,this.Site_Heat_Load_Remarks,this.Site_Type,this.SMPS_make,
      this.SMPS_Rating,this.SMPS_Remarks,this.Solar_Current,this.Solar_Remarks,this.Solar_Voltage ).map(res => <any>res.json())
    .subscribe(data => {
      this.loading.dismiss();
   console.log(data);
      if(data.Status == "pass"){

        // this.toast(data.responseMessage +'Your Otp is:'+data.otp);
      }
      else
      {
        this.toast("Login Failed");
      }
    },
    errData =>{
      this.toast("Please Check Your Internet Connection");
      this.loading.dismiss();

     });

  }

  showAlert(msg) {
    let alert = this.alertCtrl.create({
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }
  toast(msg){
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 1000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }
  showLoader(msg) {
    this.loading = this.loadingCtrl.create({
      content: msg
    });
    this.loading.present();
  }


}
