import { Component } from '@angular/core';
import { IonicPage, NavController, NavParams,MenuController,AlertController, ToastController,LoadingController } from 'ionic-angular';
import { FormGroup,FormControl, Validators } from '@angular/forms';
import { ApiProvider } from '../../Api/api';
import { OtpPage } from '../otp/otp';
import { ChangepasswordPageModule } from '../changepassword/changepassword.module';
import { TabsPage } from '../tabs/tabs';


/**
 * Generated class for the MobilePage page.
 *
 * See https://ionicframework.com/docs/components/#navigation for more info on
 * Ionic pages and navigation.
 */

@IonicPage()
@Component({
  selector: 'page-changepassword',
  templateUrl: 'changepassword.html',
})
export class ChangepasswordPage {
  myForm: FormGroup;
  password:any;
  cpassword:any;

  loading:any;

  constructor(public navCtrl: NavController, public navParams: NavParams,public menu: MenuController,
    public alertCtrl:AlertController, private toastCtrl:ToastController,
    public loadingCtrl: LoadingController,public service:ApiProvider) {
    this.menu.swipeEnable(false);
    this.myForm = new FormGroup({ password: new FormControl(null, Validators.compose([Validators.required,Validators.minLength(5)])) ,
      cpassword: new FormControl(null, Validators.compose([Validators.required,Validators.minLength(5)]))
    });
  }
  
  validate(): boolean {
    if (this.myForm.valid) {
      return true;
    }
    // figure out the error message
    let errorMsg = '';
    // validate each field
    let control = this.myForm.controls['password'];
    let control1 = this.myForm.controls['cpassword'];

    if (!control.valid) {
      if (control.errors['required']) {
        errorMsg = 'Provide a password please';
        this.showAlert(errorMsg);
        return;        
      } else if (control.errors['minlength']) {
        errorMsg = 'The password must have at least 5 numbers';
        this.showAlert(errorMsg);
        return;        
      }
    }
    if (!control1.valid) {
      if (control1.errors['required']) {
        errorMsg = 'Provide a confirm password please';
        this.showAlert(errorMsg);
        return;        
      } else if (this.password == this.cpassword) {
        errorMsg = 'The password and confirm password must be same';
        this.showAlert(errorMsg);
        return;        
      }
    }
    return false;
  }


  onSubmit(form: any) {
    if (this.validate()) {
      if (this.password == this.cpassword)
      {
        this.showLoader('Please wait...');
        let userId = this.navParams.get('id');
        // let id = localStorage.getItem('ID');
  
        // process the data
        this.service.UpdateUser( userId,this.password,'' ).map(res => <any>res.json())
        .subscribe(data => {
          this.loading.dismiss();
       console.log(data);
       if(data.Status == "pass"){
        this.toast("Password set successfully");
        let userId = this.navParams.get('id');
         localStorage.setItem('ID',userId);

        this.navCtrl.setRoot(TabsPage);

       }
       else{

       }
  
  
          // if(data.OTP == "0"){
  
          //   // this.toast(data.responseMessage +'Your Otp is:'+data.otp);
          //   this.toast("Invalid Mobile No.!!")
          // }
          // else
          // {
          //   localStorage.setItem('otp',data.OTP);
          //   localStorage.setItem('mobile',this.mobile);
  
          //   this.toast(data.OTP);
          //   this.navCtrl.setRoot(OtpPage);
          // }
        },
        errData =>{
          this.toast("Please Check Your Internet Connection");
          this.loading.dismiss();
  
         });
  
      }
      else
      {
        this.toast("The password and confirm password must be same")

      }

    }
  }


  showAlert(msg) {
    let alert = this.alertCtrl.create({
      subTitle: msg,
      buttons: ['OK']
    });
    alert.present();
  }
  toast(msg){
    let toast = this.toastCtrl.create({
      message: msg,
      duration: 3000,
      position: 'bottom'
    });
    toast.onDidDismiss(() => {
      console.log('Dismissed toast');
    });
    toast.present();
  }
  showLoader(msg) {
    this.loading = this.loadingCtrl.create({
      content: msg
    });
    this.loading.present();
  }
  forgotPassword()
  {
  }
  backBtn()
  {
  }
}
